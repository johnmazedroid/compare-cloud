export const CloudProvidersMockData = [{
    "cloudProviderId": "1",
    "name": "AWS",
    "description": "Amazon Web Services",
    "displayOrder": 1
  },
  {
    "cloudProviderId": "2",
    "name": "Azure",
    "description": "Microsoft Azure",
    "displayOrder": 2
  },
  {
    "cloudProviderId": "3",
    "name": "Google",
    "description": "Google",
    "displayOrder": 3
  }];
