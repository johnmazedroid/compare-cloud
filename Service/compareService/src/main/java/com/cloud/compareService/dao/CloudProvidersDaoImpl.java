package com.cloud.compareService.dao;

import com.cloud.compareService.model.CloudProvider;
import com.cloud.compareService.model.Region;
import com.cloud.compareService.repository.InstanceFamilyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.cloud.compareService.repository.CloudProviderRepository;


import java.util.List;
@Component
public class CloudProvidersDaoImpl extends BaseDao<CloudProvider> {

    @Autowired
    public CloudProvidersDaoImpl(CloudProviderRepository cloudProvidersRepository){
        super(cloudProvidersRepository);
    }

    public CloudProvider saveCloudProvider(CloudProvider cloudProvider) {
        return repository.save(cloudProvider);
    }

    public CloudProvider findCloudProviderById(int id) {
        return repository.findById(id);
    }
    public List<CloudProvider> findCloudProviderByName(String  name) {
        return ((CloudProviderRepository)repository).findByName(name);
    }

    public List<CloudProvider> findAll() {
        return repository.findAll();
    }

}




