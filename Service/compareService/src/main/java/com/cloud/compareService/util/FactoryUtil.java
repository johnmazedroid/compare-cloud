package com.cloud.compareService.util;

import com.cloud.compareService.dao.ApplicationPropertiesDaoImpl;
import com.cloud.compareService.dao.BaseDao;
import com.cloud.compareService.dao.InstanceDaoImpl;
import com.cloud.compareService.dao.InstanceFamilyDaoImpl;
import com.cloud.compareService.dao.InstanceTypeDaoImpl;
import com.cloud.compareService.dao.LeaseTermDaoImpl;
import com.cloud.compareService.dao.OfferingClassDaoImpl;
import com.cloud.compareService.dao.OperatingSystemDaoImpl;
import com.cloud.compareService.dao.PaymentOptionDaoImpl;
import com.cloud.compareService.dao.ProcessorDaoImpl;
import com.cloud.compareService.dao.RegionDaoImpl;
import com.cloud.compareService.model.CloudProvider;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactoryUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class FactoryUtil {

  private static final Logger LOG = LoggerFactory.getLogger(FactoryUtil.class);

  private List<CloudProvider> cloudProviderList;

  @Autowired
  ApplicationPropertiesDaoImpl applicationPropertiesDao;

  @Autowired
  FactoryUtil customFactoryUtil;

  @Autowired
  private ApplicationContext applicationContext;

  public  BaseDao getDaoByName(String name){
    BaseDao baseDao;
    switch (name) {
      case "region":
        baseDao = BeanFactoryUtils.beanOfType(applicationContext, RegionDaoImpl.class);
        break;
      case "instance":
        baseDao = BeanFactoryUtils.beanOfType(applicationContext, InstanceDaoImpl.class);
        break;
      case "instanceType":
        baseDao = BeanFactoryUtils.beanOfType(applicationContext, InstanceTypeDaoImpl.class);
        break;
      case "instanceFamily":
        baseDao = BeanFactoryUtils.beanOfType(applicationContext, InstanceFamilyDaoImpl.class);
        break;
      case "processor":
        baseDao = BeanFactoryUtils.beanOfType(applicationContext, ProcessorDaoImpl.class);
        break;
      case "operatingSystem":
        baseDao = BeanFactoryUtils.beanOfType(applicationContext, OperatingSystemDaoImpl.class);
        break;
      case "leaseTerm":
        baseDao = BeanFactoryUtils.beanOfType(applicationContext, LeaseTermDaoImpl.class);
        break;
      case "offeringClass":
        baseDao = BeanFactoryUtils.beanOfType(applicationContext, OfferingClassDaoImpl.class);
        break;
      case "paymentOption":
        baseDao = BeanFactoryUtils.beanOfType(applicationContext, PaymentOptionDaoImpl.class);
        break;
      default:
        baseDao =null;
        break;
    }
    return baseDao;
  }






}
