package com.cloud.compareService.dao;

import com.cloud.compareService.model.CloudProvider;
import com.cloud.compareService.model.Region;
import com.cloud.compareService.repository.CloudProviderRepository;
import com.cloud.compareService.repository.RegionRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

@Component
public class RegionDaoImpl extends BaseDao<Region> {



    @Autowired
    public RegionDaoImpl(RegionRepository regionRepository){
        super(regionRepository);
    }


    public Region saveCloudProvider(Region region) {
        return repository.save(region);
    }

    public Region findRegionById(int id) {
        return repository.findById(id);
    }
    public List<Region> findCloudProviderByName(String  cloudProviderId,String region) {
        return ((RegionRepository)repository).findByCloudProviderAndRegion(cloudProviderId,region);
    }

    public List<Region> findAll(Specification spec) {
        return repository.findAll(spec);
    }

    public List<Region> findAll() {
        return repository.findAll();
    }

}




