package com.cloud.compareService.dao;

import com.cloud.compareService.model.OperatingSystem;
import com.cloud.compareService.model.Region;
import com.cloud.compareService.repository.OperatingSystemRepository;
import com.cloud.compareService.repository.PaymentOptionRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OperatingSystemDaoImpl extends BaseDao<OperatingSystem>{

    @Autowired
    public OperatingSystemDaoImpl(OperatingSystemRepository operatingSystemRepository){
        super(operatingSystemRepository);
    }

    public OperatingSystem save(OperatingSystem operatingSystem) {
        return repository.save(operatingSystem);
    }

    public List<OperatingSystem> findAll() {
        return repository.findAll();
    }

}




