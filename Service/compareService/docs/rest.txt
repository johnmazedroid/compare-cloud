http://localhost:8011/compare/computeResource

Input:
  Header
    "Content-Type":"application/json"
  Body:
    [
       {"workload" :
        [
        {"key":"cloudProvider","value":["AWS"],"search":"EQUALITY"},
    	{"key":"region","value":["US EAST1"],"search":"EQUALITY"},
    	{"key":"instance","value":["c5d.9xlarge"],"search":"EQUALITY"},
    	{"key":"instanceType","value":["DEFAULT"],"search":"EQUALITY"},
    	{"key":"instanceFamily","value":["DEFAULT"],"search":"EQUALITY"},
        {"key":"operatingSystem","value":["DEFAULT"],"search":"EQUALITY"},
    	{"key":"processor","value":["DEFAULT"],"search":"EQUALITY"},
    	{"key":"noOfServers","value":[""],"search":"EQUALITY"},
    	{"key":"memory","value":["72"],"search":"EQUALITY"},
        {"key":"defaultStorage","value":[""],"search":"EQUALITY"},
    	{"key":"virtualCpu","value":["36"],"search":"EQUALITY"},
    	{"key":"leaseTerm","value":["1yr"],"search":"EQUALITY"},
    	{"key":"offeringClass","value":["Convertible"],"search":"EQUALITY"},
    	{"key":"paymentOption","value":["No Upfront"],"search":"EQUALITY"}
    ]
    },
      {"workload" : [
        {"key":"cloudProvider","value":["AZURE"],"search":"EQUALITY"},
    	{"key":"region","value":["US EAST1"],"search":"EQUALITY"},
    	{"key":"instance","value":["c5d.9xlarge"],"search":"EQUALITY"},
    	{"key":"instanceType","value":["DEFAULT"],"search":"EQUALITY"},
    	{"key":"instanceFamily","value":["DEFAULT"],"search":"EQUALITY"},
        {"key":"operatingSystem","value":["DEFAULT"],"search":"EQUALITY"},
    	{"key":"processor","value":["DEFAULT"],"search":"EQUALITY"},
    	{"key":"noOfServers","value":[""],"search":"EQUALITY"},
    	{"key":"memory","value":["72"],"search":"EQUALITY"},
        {"key":"defaultStorage","value":[""],"search":"EQUALITY"},
    	{"key":"virtualCpu","value":["36"],"search":"EQUALITY"},
    	{"key":"leaseTerm","value":["1yr"],"search":"EQUALITY"},
    	{"key":"offeringClass","value":["Convertible"],"search":"EQUALITY"},
    	{"key":"paymentOption","value":["No Upfront"],"search":"EQUALITY"}
    ]
      }
    ]

====================================================================================================

FIND-ALL

http://compare-cloud/data/application-properties/all
http://compare-cloud/data/cloud-provider/all
http://compare-cloud/data/instance/all
http://compare-cloud/data/instance-family/all
http://compare-cloud/data/lease-term/all
http://compare-cloud/data/offering-class/all
http://compare-cloud/data/operating-system/all
http://compare-cloud/data/payment-option/all
http://compare-cloud/data/processor/all
http://compare-cloud/data/region/all
http://compare-cloud/data/server-attributes/all
http://compare-cloud/data/server-pricing/all
====================================================================================================