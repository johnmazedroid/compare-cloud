import React from 'react';
import { PropTypes } from 'prop-types';

import Step from './Step';

const Stepper = ({ activeStep, steps, disabledSteps }) => {
  return (
    <div className="stepper-root">
      <div className="stepper">
        {steps.map((step, index) => (
          <Step
            key={index}
            width={100 / steps.length}
            title={step.title}
            icon={step.icon}
            href={step.href}
            onClick={step.onClick}
            active={
              !(disabledSteps || []).includes(index) && index === activeStep
            }
            completed={
              !(disabledSteps || []).includes(index) && index < activeStep
            }
            first={index === 0}
            isLast={index === steps.length - 1}
            index={index}
          />
        ))}
      </div>
    </div>
  );
};

Stepper.defaultProps = {
  activeStep: 0,
};

Stepper.propTypes = {
  activeStep: PropTypes.number,
  steps: PropTypes.array,
  disabledSteps: PropTypes.array
};

export default Stepper;
