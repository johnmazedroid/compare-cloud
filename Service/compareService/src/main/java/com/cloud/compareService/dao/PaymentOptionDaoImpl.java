package com.cloud.compareService.dao;

import com.cloud.compareService.model.PaymentOption;
import com.cloud.compareService.model.Region;
import com.cloud.compareService.repository.PaymentOptionRepository;
import com.cloud.compareService.repository.ProcessorRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PaymentOptionDaoImpl extends BaseDao<PaymentOption>{

    @Autowired
    public PaymentOptionDaoImpl(PaymentOptionRepository paymentOptionRepository){
        super(paymentOptionRepository);
    }

    public PaymentOption save(PaymentOption paymentOption) {
        return repository.save(paymentOption);
    }

    public List<PaymentOption> findAll() {
        return repository.findAll();
    }

}




