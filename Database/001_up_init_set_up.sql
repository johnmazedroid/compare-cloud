CREATE  SCHEMA IF NOT EXISTS `users`

CREATE TABLE `comparecloud`.`cloud_provider`(
   `id` INT NOT NULL AUTO_INCREMENT,
   `name` VARCHAR(40) NOT NULL,
   `description` VARCHAR(100) NOT NULL, 
   `active` BOOLEAN DEFAULT FALSE,
   `display_order` INT, 
   `created_date`     DATETIME DEFAULT CURRENT_TIMESTAMP,
   `created_by` VARCHAR(40) NOT NULL DEFAULT 'admin',
    `updated_date` DATETIME ON UPDATE CURRENT_TIMESTAMP,
    `updated_by` VARCHAR(40) ,
   PRIMARY KEY ( id )
);

CREATE TABLE `users`.`user`(
   `id` INT NOT NULL AUTO_INCREMENT,
   `name` VARCHAR(40) NOT NULL,
   `password` VARCHAR(40) NOT NULL DEFAULT 'password',
   `description` VARCHAR(100) NULL,	
   `active` BOOLEAN DEFAULT FALSE,
   `created_date`     DATETIME DEFAULT CURRENT_TIMESTAMP,
   `created_by` VARCHAR(40) NOT NULL DEFAULT 'admin',
    `updated_date` DATETIME ON UPDATE CURRENT_TIMESTAMP,
    `updated_by` VARCHAR(40) ,
   PRIMARY KEY ( id )
);

