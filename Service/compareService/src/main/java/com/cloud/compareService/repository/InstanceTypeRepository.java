package com.cloud.compareService.repository;

import com.cloud.compareService.model.InstanceFamily;
import com.cloud.compareService.model.InstanceType;
import com.cloud.compareService.model.LeaseTerm;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;


@Repository
public interface InstanceTypeRepository extends JpaRepository<InstanceType, Integer>
    , JpaSpecificationExecutor<InstanceType>,BaseRepository<InstanceType>{

    public InstanceType findById(int Id);
    
    public List<InstanceType> findAll();

}
