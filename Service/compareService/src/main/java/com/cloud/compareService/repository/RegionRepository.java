package com.cloud.compareService.repository;

import com.cloud.compareService.model.CloudProvider;
import com.cloud.compareService.model.Region;
import com.cloud.compareService.model.ServerAttributes;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface RegionRepository extends JpaRepository<Region, Integer>,
    JpaSpecificationExecutor<Region>,BaseRepository<Region> {

    public Region findById(int Id);

    @Query(value=" select * from region_mst \n" +
            "WHERE cloud_provider_id = :cloudProviderId and region LIKE :regions", nativeQuery=true)
    public List<Region> findByCloudProviderAndRegion(@Param("cloudProviderId") String cloudProviderId, @Param("region") String region );

    public List<Region> findAll(Specification spec);
}
