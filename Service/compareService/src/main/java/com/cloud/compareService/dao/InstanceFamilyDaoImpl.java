package com.cloud.compareService.dao;

import com.cloud.compareService.model.Instance;
import com.cloud.compareService.model.InstanceFamily;
import com.cloud.compareService.model.Region;
import com.cloud.compareService.repository.InstanceFamilyRepository;
import com.cloud.compareService.repository.InstanceTypeRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class InstanceFamilyDaoImpl extends BaseDao<InstanceFamily>{

    @Autowired
    public InstanceFamilyDaoImpl(InstanceFamilyRepository instanceFamilyRepository){
        super(instanceFamilyRepository);
    }

    public InstanceFamily save(InstanceFamily instanceFamily) {
        return repository.save(instanceFamily);
    }

    public List<InstanceFamily> findAll() {
        return repository.findAll();
    }

}




