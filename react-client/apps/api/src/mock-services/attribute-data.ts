import * as express from 'express';

import { InstanceTypeMockData } from './data/mock-instance-type';
import { OperatingSystemMockData } from './data/mock-operating-system';
import { InstanceFamilyMockData } from './data/mock-instance-family';
import { RegionMockData } from './data/mock-region';

const DataByAttributeNameMockRouter: express.Router = express.Router();

DataByAttributeNameMockRouter.get('/cloud-api/data/getUniqueNameBycp',
  (req, res) => {
  console.log('============================= GET DATA BY ATTRIBUTE NAME - '+ req.query.attribute + ' ==================================');
  console.log('request method :::', req.method);
  console.log('request url :::', req.url);
  console.log('request params :::', req.params);
  console.log('request params :::', req.query);
  console.log('request params :::', req.body);
  console.log('============================= GET DATA BY ATTRIBUTE NAME ==================================');
  if(req.query.attribute === 'instanceType') {
    res.send(InstanceTypeMockData);
  } else if(req.query.attribute === 'operatingSystem') {
    res.send(OperatingSystemMockData);
  } else if(req.query.attribute === 'instanceFamily') {
    res.send(InstanceFamilyMockData);
  } else if(req.query.attribute === 'region') {
    res.send(RegionMockData);
  }
});

export default DataByAttributeNameMockRouter;
