import { axiosApiInstance } from '../HttpInterceptors';

export const getDataByAttributeNameHTTPService = (context) => {
  console.log('getDataByAttributeNameHTTPInterface');
  const requestParams = {
    params: {...context}
  }

  return new Promise((resolve, reject) => {
    axiosApiInstance.get('/cloud-api/data/getUniqueNameBycp', requestParams)
    .then((response) => resolve(response.data))
    .catch((error) => reject(error));
  });
}
