INSERT INTO server_attributes_tmp (
	cloud_provider_id
	,operating_system_mst_id
	,region_mst_id
	,instance_mst_id
	,instance_type_mst_id
	,instance_family_mst_id
	,processor_mst_id
	,virtual_cpu
	,memory
	,default_storage
	,server_attributes_id
	)
SELECT cloud_provider_id
	,operating_system_mst_id
	,region_mst_id
	,instance_mst_id
	,instance_type_mst_id
	,instance_family_mst_id
	,processor_mst_id
	,virtual_cpu
	,memory
	,default_storage
	,server_attributes_id
FROM server_attributes_stg;

INSERT INTO server_pricing_tmp (
	server_attributes_id
	,lease_term_mst_id
	,offering_class_mst_id
	,payment_option_mst_id
	,hourly_price
	,server_pricing_id
	)
SELECT server_attributes_id
	,lease_term_mst_id
	,offering_class_mst_id
	,payment_option_mst_id
	,hourly_price
	,server_pricing_id
FROM server_pricing_stg
WHERE server_attributes_id IS NOT NULL;