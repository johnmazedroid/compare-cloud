package com.cloud.compareService.repository;

import com.cloud.compareService.model.Instance;
import com.cloud.compareService.model.InstanceType;
import com.cloud.compareService.model.Region;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;


@Repository
public interface InstanceRepository extends JpaRepository<Instance, Integer>
    , JpaSpecificationExecutor<Instance>,BaseRepository<Instance>{

    public Instance findById(int Id);
    
    public List<Instance> findAll();

}
