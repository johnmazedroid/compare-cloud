ALTER TABLE server_attributes_tmp DROP FOREIGN KEY server_attributes_tmp_cp_fk1;
ALTER TABLE server_attributes_tmp DROP FOREIGN KEY server_attributes_tmp_os_fk2;
ALTER TABLE server_attributes_tmp DROP FOREIGN KEY server_attributes_tmp_re_fk3;
ALTER TABLE server_attributes_tmp DROP FOREIGN KEY server_attributes_tmp_in_fk4;
ALTER TABLE server_attributes_tmp DROP FOREIGN KEY server_attributes_tmp_it_fk5;
ALTER TABLE server_attributes_tmp DROP FOREIGN KEY server_attributes_tmp_if_fk6;
ALTER TABLE server_attributes_tmp DROP FOREIGN KEY server_attributes_tmp_pr_fk7;

ALTER TABLE server_pricing_tmp DROP FOREIGN KEY server_pricing_tmp_lt_fk1;
ALTER TABLE server_pricing_tmp DROP FOREIGN KEY server_pricing_tmp_oc_fk2;
ALTER TABLE server_pricing_tmp DROP FOREIGN KEY server_pricing_tmp_po_fk3;
ALTER TABLE server_pricing_tmp DROP FOREIGN KEY server_pricing_tmp_sa_fk4;

DROP TABLE IF EXISTS server_pricing;
DROP TABLE IF EXISTS server_attributes;

RENAME TABLE server_attributes_tmp TO server_attributes;
RENAME TABLE server_pricing_tmp TO server_pricing;

ALTER TABLE server_attributes ADD CONSTRAINT `server_attributes_cp_fk1` FOREIGN KEY (`cloud_provider_id`)       REFERENCES `cloud_provider_mst` (`cloud_provider_id`);
ALTER TABLE server_attributes ADD CONSTRAINT `server_attributes_os_fk2` FOREIGN KEY (`operating_system_mst_id`) REFERENCES `operating_system_mst` (`operating_system_mst_id`);
ALTER TABLE server_attributes ADD CONSTRAINT `server_attributes_re_fk3` FOREIGN KEY (`region_mst_id`)           REFERENCES `region_mst` (`region_mst_id`);
ALTER TABLE server_attributes ADD CONSTRAINT `server_attributes_in_fk4` FOREIGN KEY (`instance_mst_id`)         REFERENCES `instance_mst` (`instance_mst_id`);
ALTER TABLE server_attributes ADD CONSTRAINT `server_attributes_it_fk5` FOREIGN KEY (`instance_type_mst_id`)    REFERENCES `instance_type_mst` (`instance_type_mst_id`);
ALTER TABLE server_attributes ADD CONSTRAINT `server_attributes_if_fk6` FOREIGN KEY (`instance_family_mst_id`)  REFERENCES `instance_family_mst` (`instance_family_mst_id`);
ALTER TABLE server_attributes ADD CONSTRAINT `server_attributes_pr_fk7` FOREIGN KEY (`processor_mst_id`)        REFERENCES `processor_mst` (`processor_mst_id`);
ALTER TABLE server_attributes CHANGE server_attributes_id server_attributes_id INT(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE server_pricing ADD CONSTRAINT `server_pricing_lt_fk1` FOREIGN KEY (`lease_term_mst_id`)     REFERENCES `lease_term_mst` (`lease_term_mst_id`);
ALTER TABLE server_pricing ADD CONSTRAINT `server_pricing_oc_fk2` FOREIGN KEY (`offering_class_mst_id`) REFERENCES `offering_class_mst` (`offering_class_mst_id`);
ALTER TABLE server_pricing ADD CONSTRAINT `server_pricing_po_fk3` FOREIGN KEY (`payment_option_mst_id`) REFERENCES `payment_option_mst` (`payment_option_mst_id`);
ALTER TABLE server_pricing ADD CONSTRAINT `server_pricing_sa_fk4` FOREIGN KEY (`server_attributes_id`)  REFERENCES `server_attributes` (`server_attributes_id`);
ALTER TABLE server_pricing CHANGE server_pricing_id server_pricing_id INT(11) NOT NULL AUTO_INCREMENT;