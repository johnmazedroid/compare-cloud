import { axiosApiInstance } from '../HttpInterceptors';

export const getCloudProvidersHTTPService = () => {
    console.log('getCloudProvidersHTTPInterface');

    // TODO: Keeping this commented code for reference. REMOVE once it done.
    // return axiosApiInstance.get('/cloud-api/data/cloud-provider/all')
    //     .then((response) => {
    //         console.log(response);
    //     })
    //     .catch((error) => {
    //         if (error.response) {
    //             // The request was made and the server responded with a status code
    //             // that falls out of the range of 2xx
    //             console.log(error.response.data);
    //             console.log(error.response.status);
    //             console.log(error.response.headers);
    //         } else if (error.request) {
    //             // The request was made but no response was received
    //             // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
    //             // http.ClientRequest in node.js
    //             console.log(error.request);
    //         } else {
    //             // Something happened in setting up the request that triggered an Error
    //             console.log('Error', error.message);
    //         }
    //     });

    return new Promise((resolve, reject) => {
        axiosApiInstance.get('/cloud-api/data/cloud-provider/all')
        .then((response) => resolve(response.data))
        .catch((error) => reject(error));
    });
}
