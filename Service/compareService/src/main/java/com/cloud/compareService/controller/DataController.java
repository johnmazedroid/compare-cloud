package com.cloud.compareService.controller;

import com.cloud.compareService.exception.ResponseException;
import com.cloud.compareService.model.ApplicationProperties;
import com.cloud.compareService.model.CloudProvider;
import com.cloud.compareService.model.Instance;
import com.cloud.compareService.model.InstanceFamily;
import com.cloud.compareService.model.LeaseTerm;
import com.cloud.compareService.model.OfferingClass;
import com.cloud.compareService.model.OperatingSystem;
import com.cloud.compareService.model.PaymentOption;
import com.cloud.compareService.model.Region;
import com.cloud.compareService.model.Processor;
import com.cloud.compareService.model.ServerAttributes;
import com.cloud.compareService.model.ServerPricing;

import com.cloud.compareService.service.DataService;
import com.cloud.compareService.util.Util;
import com.cloud.compareService.vo.ComputeResourceOutputVO;
import com.cloud.compareService.vo.NameWithCloudProviderOutputVO;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/data")
public class DataController {

    private static final Logger LOG = LoggerFactory.getLogger(DataController.class);

    @Value("${spring.datasource.url}")
    private String databaseUrl;


    @Autowired
    private DataService dataService;

    @RequestMapping(method=RequestMethod.GET, path="/getUniqueAttributeList")
    @ResponseBody
    public Set<String> getUniqueAttributeList(@RequestParam Map<String, String> requestParams)
    throws ResponseException {
        String field=requestParams.get("field");
        LOG.info("The input field is " +  field);
         return dataService.getUniqueAttributeList(field);
    }

    @RequestMapping(method=RequestMethod.GET, path="/getUniqueNameBycp")
    @ResponseBody
    public List<NameWithCloudProviderOutputVO> getUniqueNameByCloudProvider(@RequestParam Map<String, String> requestParams)
        throws ResponseException{
        return dataService.getUniqueNameByCloudProvider( requestParams);
    }



    @RequestMapping(method=RequestMethod.GET, path="/application-properties/all")
    @ResponseBody
    public List<ApplicationProperties> getApplicationProperties() {
        System.out.println("databaseUrl " +  databaseUrl);
        return dataService.getApplicationProperties();
    }

    @RequestMapping(method=RequestMethod.GET, path="/cloud-provider/all")
    @ResponseBody
    public List<CloudProvider> getCloudProvider() {
        return dataService.getCloudProvider();
    }

    @RequestMapping(method=RequestMethod.GET, path="/instance/all")
    @ResponseBody
    public List<Instance> getInstance() {
        return dataService.getInstance();
    }

    @RequestMapping(method=RequestMethod.GET, path="/instance-family/all")
    @ResponseBody
    public List<InstanceFamily> getInstanceFamily() {
        return dataService.getInstanceFamily();
    }

    @RequestMapping(method=RequestMethod.GET, path="/lease-term/all")
    @ResponseBody
    public List<LeaseTerm> getLeaseTerm() {
        return dataService.getLeaseTerm();
    }

    @RequestMapping(method=RequestMethod.GET, path="/offering-class/all")
    @ResponseBody
    public List<OfferingClass> getOfferingClass() {
        return dataService.getOfferingClass();
    }

    @RequestMapping(method=RequestMethod.GET, path="/operating-system/all")
    @ResponseBody
    public List<OperatingSystem> getOperatingSystem() {
        return dataService.getOperatingSystem();
    }

    @RequestMapping(method=RequestMethod.GET, path="/payment-option/all")
    @ResponseBody
    public List<PaymentOption> getPaymentOption() {
        return dataService.getPaymentOption();
    }

    @RequestMapping(method=RequestMethod.GET, path="/processor/all")
    @ResponseBody
    public List<Processor> getProcessor() {
        return dataService.getProcessor();
    }

    @RequestMapping(method=RequestMethod.GET, path="/region/all")
    @ResponseBody
    public List<Region> getRegion() {
        System.out.println("databaseUrl " +  databaseUrl);
        return dataService.getRegion();
    }



    @RequestMapping(method=RequestMethod.GET, path="/server-attributes/all")
    @ResponseBody
    public List<ServerAttributes> getServerAttributes() {
        return dataService.getServerAttributes();
    }

    @RequestMapping(method=RequestMethod.GET, path="/server-pricing/all")
    @ResponseBody
    public List<ServerPricing> getServerPricing() {
        return dataService.getServerPricing();
    }



}
