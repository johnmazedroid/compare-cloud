import { Checkbox } from 'antd';
import { CheckboxGroupProps } from 'antd/lib/checkbox';
import React, { ReactElement } from 'react';

import './check-box-group.module.scss';

export interface InputCheckBoxGroupProps extends CheckboxGroupProps {
  customProperties: {
    showLabel?: boolean;
    label?: string
  }
}

export const CheckBoxGroup = (props: InputCheckBoxGroupProps): ReactElement => {
  const getDefaultProperties = (propsIn: InputCheckBoxGroupProps): CheckboxGroupProps => {
    const defaultProperties = {
      ...propsIn
    }
    delete defaultProperties.customProperties; // Delete Custom Properties and make sure to default flow works fine
    return {
      ...defaultProperties
    }
  }

  const defaultProperties: CheckboxGroupProps = getDefaultProperties(props);
  return (
    <>
      {props.customProperties.showLabel && <label className="m-0">{props.customProperties.label}</label>}
      <Checkbox.Group {...defaultProperties} />
    </>
  );
}
