export const RegionMockData = [
  {
    "name": "US East (Northern Virginia)\r\n",
    "cloudProviderIdList": [
      "1"
    ]
  },
  {
    "name": "US East (Ohio)\r\n",
    "cloudProviderIdList": [
      "1"
    ]
  },
  {
    "name": "Cape Town",
    "cloudProviderIdList": [
      "1"
    ]
  },
  {
    "name": "Hong Kong",
    "cloudProviderIdList": [
      "1"
    ]
  },
  {
    "name": "Tokyo",
    "cloudProviderIdList": [
      "1"
    ]
  },
  {
    "name": "Seoul",
    "cloudProviderIdList": [
      "1"
    ]
  },
  {
    "name": "AP North East\r\n",
    "cloudProviderIdList": [
      "1"
    ]
  },
  {
    "name": "Mumbai\r\n",
    "cloudProviderIdList": [
      "1"
    ]
  },
  {
    "name": "Singapore\r\n",
    "cloudProviderIdList": [
      "1"
    ]
  },
  {
    "name": "Sydney\r\n",
    "cloudProviderIdList": [
      "1"
    ]
  },
  {
    "name": "Canada Central\r\n",
    "cloudProviderIdList": [
      "1",
      "2"
    ]
  },
  {
    "name": "Frankfurt\r\n",
    "cloudProviderIdList": [
      "1"
    ]
  },
  {
    "name": "Stockholm\r\n",
    "cloudProviderIdList": [
      "1"
    ]
  },
  {
    "name": "Milan\r\n",
    "cloudProviderIdList": [
      "1"
    ]
  },
  {
    "name": "Ireland\r\n",
    "cloudProviderIdList": [
      "1"
    ]
  },
  {
    "name": "London\r\n",
    "cloudProviderIdList": [
      "1"
    ]
  },
  {
    "name": "Paris\r\n",
    "cloudProviderIdList": [
      "1"
    ]
  },
  {
    "name": "Bahrain\r\n",
    "cloudProviderIdList": [
      "1"
    ]
  },
  {
    "name": "Sao Paulo\r\n",
    "cloudProviderIdList": [
      "1"
    ]
  },
  {
    "name": "US Gov East",
    "cloudProviderIdList": [
      "1"
    ]
  },
  {
    "name": "US Gov West\r\n",
    "cloudProviderIdList": [
      "1"
    ]
  },
  {
    "name": "US West (Northern California)\r\n",
    "cloudProviderIdList": [
      "1"
    ]
  },
  {
    "name": "US West (Oregon)\r\n",
    "cloudProviderIdList": [
      "1"
    ]
  },
  {
    "name": "AP East\r\n",
    "cloudProviderIdList": [
      "2"
    ]
  },
  {
    "name": "AP South East\r\n",
    "cloudProviderIdList": [
      "2"
    ]
  },
  {
    "name": "Australia Central\r\n",
    "cloudProviderIdList": [
      "2"
    ]
  },
  {
    "name": "Australia Central 2\r\n",
    "cloudProviderIdList": [
      "2"
    ]
  },
  {
    "name": "Australia East\r\n",
    "cloudProviderIdList": [
      "2"
    ]
  },
  {
    "name": "Australia South East\r\n",
    "cloudProviderIdList": [
      "2"
    ]
  },
  {
    "name": "Brazil South\r\n",
    "cloudProviderIdList": [
      "2"
    ]
  },
  {
    "name": "Brazil South East\r\n",
    "cloudProviderIdList": [
      "2"
    ]
  },
  {
    "name": "Canada East\r\n",
    "cloudProviderIdList": [
      "2"
    ]
  },
  {
    "name": "India Central\r\n",
    "cloudProviderIdList": [
      "2"
    ]
  },
  {
    "name": "Europe North\r\n",
    "cloudProviderIdList": [
      "2"
    ]
  },
  {
    "name": "Europe West\r\n",
    "cloudProviderIdList": [
      "2"
    ]
  },
  {
    "name": "France Central\r\n",
    "cloudProviderIdList": [
      "2"
    ]
  },
  {
    "name": "France South\r\n",
    "cloudProviderIdList": [
      "2"
    ]
  },
  {
    "name": "Germany Central\r\n",
    "cloudProviderIdList": [
      "2"
    ]
  },
  {
    "name": "Germany North\r\n",
    "cloudProviderIdList": [
      "2"
    ]
  },
  {
    "name": "Germany Northeast\r\n",
    "cloudProviderIdList": [
      "2"
    ]
  },
  {
    "name": "Germany West Central\r\n",
    "cloudProviderIdList": [
      "2"
    ]
  },
  {
    "name": "Japan East\r\n",
    "cloudProviderIdList": [
      "2"
    ]
  },
  {
    "name": "Japan West\r\n",
    "cloudProviderIdList": [
      "2"
    ]
  },
  {
    "name": "Korea Central\r\n",
    "cloudProviderIdList": [
      "2"
    ]
  },
  {
    "name": "Korea South\r\n",
    "cloudProviderIdList": [
      "2"
    ]
  },
  {
    "name": "Norway East\r\n",
    "cloudProviderIdList": [
      "2"
    ]
  },
  {
    "name": "Norway West\r\n",
    "cloudProviderIdList": [
      "2"
    ]
  },
  {
    "name": "South Africa North\r\n",
    "cloudProviderIdList": [
      "2"
    ]
  },
  {
    "name": "South Africa West\r\n",
    "cloudProviderIdList": [
      "2"
    ]
  },
  {
    "name": "India South\r\n",
    "cloudProviderIdList": [
      "2"
    ]
  },
  {
    "name": "Switzerland North\r\n",
    "cloudProviderIdList": [
      "2"
    ]
  },
  {
    "name": "Switzerland West\r\n",
    "cloudProviderIdList": [
      "2"
    ]
  },
  {
    "name": "UAE Central\r\n",
    "cloudProviderIdList": [
      "2"
    ]
  },
  {
    "name": "UAE North\r\n",
    "cloudProviderIdList": [
      "2"
    ]
  },
  {
    "name": "UK South\r\n",
    "cloudProviderIdList": [
      "2"
    ]
  },
  {
    "name": "UK West\r\n",
    "cloudProviderIdList": [
      "2"
    ]
  },
  {
    "name": "US Central\r\n",
    "cloudProviderIdList": [
      "2"
    ]
  },
  {
    "name": "US East\r\n",
    "cloudProviderIdList": [
      "2"
    ]
  },
  {
    "name": "US East 2\r\n",
    "cloudProviderIdList": [
      "2"
    ]
  },
  {
    "name": "US North Central\r\n",
    "cloudProviderIdList": [
      "2"
    ]
  },
  {
    "name": "US South Central\r\n",
    "cloudProviderIdList": [
      "2"
    ]
  },
  {
    "name": "US West\r\n",
    "cloudProviderIdList": [
      "2"
    ]
  },
  {
    "name": "US West 2\r\n",
    "cloudProviderIdList": [
      "2"
    ]
  },
  {
    "name": "US West Central\r\n",
    "cloudProviderIdList": [
      "2"
    ]
  },
  {
    "name": "US Gov Arizona\r\n",
    "cloudProviderIdList": [
      "2"
    ]
  },
  {
    "name": "US Gov Texas\r\n",
    "cloudProviderIdList": [
      "2"
    ]
  },
  {
    "name": "US Gov Virginia\r\n",
    "cloudProviderIdList": [
      "2"
    ]
  },
  {
    "name": "India West\r\n",
    "cloudProviderIdList": [
      "2"
    ]
  },
  {
    "name": "DEFAULT",
    "cloudProviderIdList": [
      "3"
    ]
  }
]
