import { CheckboxValueType } from "antd/lib/checkbox/Group";

export const getDataByCloudProvider = (arrayIn: any[], identifierArray: CheckboxValueType[]) => {
    const filteredData = arrayIn
        .filter(item => (item.cloudProviderIdList.length === identifierArray.length))  // Check the length
        .map(item => {
          return {
            ...item,
            isAvailable: item.cloudProviderIdList.map(itemValue => identifierArray.includes(itemValue))
          }
        })
        .filter(filterBy => {
          return !filterBy.isAvailable.includes(false);
        })
        .map(val => ({label: val.name, value: val.name}));

    return filteredData;
}