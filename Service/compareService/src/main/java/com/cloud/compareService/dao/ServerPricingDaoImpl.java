package com.cloud.compareService.dao;

import com.cloud.compareService.model.Region;
import com.cloud.compareService.model.ServerPricing;
import com.cloud.compareService.repository.RegionRepository;
import com.cloud.compareService.repository.ServerPricingRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ServerPricingDaoImpl  extends BaseDao<ServerPricing>{

    @Autowired
    public ServerPricingDaoImpl(ServerPricingRepository serverPricingRepository){
        super(serverPricingRepository);
    }

    public List<ServerPricing> findAll() {
        return repository.findAll();
    }



}
