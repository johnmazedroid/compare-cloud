export const errorMessage = (error) => {
    const errorDetail = new Error(error);
    return errorDetail.message ? errorDetail.message : 'Unexpected Error';
}