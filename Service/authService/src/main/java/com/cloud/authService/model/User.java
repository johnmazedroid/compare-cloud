package com.cloud.authService.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;


@AllArgsConstructor
@Builder
@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Entity
@Table(name = "user", schema="users")
public class User implements Serializable{
    @Id
    private int id;
    private String name;
    private String password;
    private String description;
    private boolean active;
    private Date createdDate;
    private String createdBy;
    private Date updatedDate;
    private String updatedBy;

    public User(int id, String name, boolean status){
        this.id=id;
        this.name=name;
        this.active=active;
    }

}
