import * as express from 'express';

import { CloudProvidersMockData } from './data/mock-cloud-provider';

const CloudProvidersMockRouter: express.Router = express.Router();

CloudProvidersMockRouter.get('/cloud-api/data/cloud-provider/all', (req, res) => {
  console.log('============================= CLOUD PROVIDER ==================================');
  console.log('request method :::', req.method);
  console.log('request url :::', req.url);
  console.log('request params :::', req.params);
  console.log('request params :::', req.body);
  console.log('============================= CLOUD PROVIDER ==================================');
  res.send(CloudProvidersMockData);
});

export default CloudProvidersMockRouter;
